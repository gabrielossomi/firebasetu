import 'package:firebasetu/sign_in.dart';
import 'package:firebasetu/signup_screen.dart';
import'package:flutter/material.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('Welcome'),
        backgroundColor: Colors.redAccent,
      ),
      body:Container(
        padding:EdgeInsets.symmetric(vertical:20,horizontal: 10),
        child: Column(
           children: [
             SizedBox(height:250),
             Text("Welcome",
             style: TextStyle(
               fontSize: 30,
               fontWeight: FontWeight.bold,
               color:  Colors.redAccent,
             ),),
             Expanded(child: Text('')),
             Container(
               width: double.infinity,
               height: 60,
               child: ElevatedButton(
                   style:ButtonStyle(backgroundColor: MaterialStateProperty.all( Colors.redAccent)),
                   onPressed:(){
                     Navigator.push(context, MaterialPageRoute(builder: (context)=>SignInScreen()));
                   },
                   child: Text(
                       "SignIn",
                     style: TextStyle(
                       fontSize: 20,
                     ),
                   ),
               ),
             ),
             SizedBox(height: 10,),
             Container(
               width:double.infinity,
                 height: 60,
                 child: ElevatedButton(
                     style: ButtonStyle(
                       backgroundColor: MaterialStateProperty.all( Colors.redAccent),

                     ),
                     onPressed: (){
                       Navigator.push(context, MaterialPageRoute(builder: (context)=>SignUpScreen()));
                     },
                     child: Text(
                         "SignUp",
                       style: TextStyle(
                         fontSize: 20,
                       ),
                     ),

                 ),
             )
           ],
          ),
        ),
      );

  }
}

