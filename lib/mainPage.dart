import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebasetu/dashboard.dart';
import 'package:firebasetu/sign_in.dart';
import 'package:firebasetu/start_screen.dart';
import 'package:flutter/material.dart';


class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   return Scaffold(
      body: StreamBuilder<User?>(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return DashBoardScreen();
            }
            else {
              return StartScreen();
            }
          }

      ),
    );
  }

}
