import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebasetu/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'dart:io';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  String downloadUrl =
      "https://png.pngtree.com/png-vector/20191026/ourlarge/pngtree-camera-icon-png-image_1871609.jpg";
  firebase_storage.FirebaseStorage _storage =
      firebase_storage.FirebaseStorage.instanceFor(
          bucket: "fir-tu-29647.appspot.com");
  bool ispassword = true;
  FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;
  User? _currentUser = FirebaseAuth.instance.currentUser;
  File? _getPickedFile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SIGN-UP-PAGE'),
        backgroundColor: Colors.redAccent,
      ),
      body: LayoutBuilder(
        builder: (context, constraint) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraint.maxHeight),
              child: IntrinsicHeight(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                  child: Column(
                    children: [
                      SizedBox(height: 50),
                      const SizedBox(
                        height: 20,
                      ),
                      InkWell(
                        onTap: () async {
                          FilePickerResult? _filePickerResult =
                              await FilePicker.platform.pickFiles(
                            type: FileType.image,
                            allowMultiple: false,
                          );
                          if (_filePickerResult == null) return;
                          _getPickedFile =
                              File(_filePickerResult.files.first.path!);

                          setState(() {});
                        },
                        child: CircleAvatar(
                          radius: 70,
                          backgroundImage: _getPickedFile == null
                              ? null
                              : Image.file(_getPickedFile!).image,
                        ),
                      ),
                      Divider(),
                      SizedBox(
                        height: 20,
                      ),
                      TextField(
                        controller: _firstNameController,
                        decoration: InputDecoration(
                          label: Text('FirstName'),
                          hintText: "Enter your FirstName",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      TextField(
                        controller: _lastNameController,
                        decoration: InputDecoration(
                          label: Text('LastName'),
                          hintText: "Enter your last Name",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      TextField(
                        controller: _emailController,
                        decoration: InputDecoration(
                          label: Text('Email'),
                          hintText: "Enter your email",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      TextField(
                        controller: _passwordController,
                        obscureText: ispassword,
                        decoration: InputDecoration(
                          label: Text('Password'),
                          hintText: "Enter your password",
                          suffixIcon: InkWell(
                            onTap: () {
                              if (ispassword == true) {
                                setState(() {
                                  ispassword = false;
                                });
                              } else
                                (setState(() {
                                  ispassword = true;
                                }));
                            },
                            child: Icon(ispassword == false
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                          border: OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Expanded(child: Text("")),
                      Container(
                        width: double.infinity,
                        height: 60,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.redAccent),
                          ),
                          onPressed: () async {
                            Map<String, dynamic> _userdata = {
                              "firstName": _firstNameController.text,
                              "lastName": _lastNameController.text,
                              "email": _emailController.text,
                            };
                            try {
                              if ((_passwordController.text.isNotEmpty) &&
                                  (_emailController.text.isNotEmpty)) {
                                await _auth
                                    .createUserWithEmailAndPassword(
                                        email: _emailController.text,
                                        password: _passwordController.text)
                                    .then((value) async {
                                  _firebaseFirestore
                                      .collection("users")
                                      .doc(value.user?.uid)
                                      .set(_userdata);
                                  try {
                                    final ref = _storage
                                        .ref(value.user?.uid)
                                        .child("/profilePhotos");
                                    await ref.putFile(_getPickedFile!);
                                    downloadUrl = await ref.getDownloadURL();
                                    setState(() {
                                      downloadUrl;
                                    });

                                    _firebaseFirestore
                                        .collection("users")
                                        .doc(value.user?.uid)
                                        .set({"profilePhoto": downloadUrl},
                                            SetOptions(merge: true));
                                  } catch (e) {
                                    print(e);
                                  }
                                });
                                ScaffoldMessenger.of(context)
                                    .hideCurrentSnackBar();
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                  content: Text("Sigup is successful"),
                                ));
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SignInScreen()));
                              }
                            } on FirebaseAuthException catch (e) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text((e.toString())),
                              ));
                            }
                          },
                          child: const Text(
                            "SignUp",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
