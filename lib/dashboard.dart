import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebasetu/sign_in.dart';
import 'package:firebasetu/signup_screen.dart';
import 'package:flutter/material.dart';

class DashBoardScreen extends StatefulWidget {
  const DashBoardScreen({Key? key}) : super(key: key);

  @override
  State<DashBoardScreen> createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;
  User? _currentUser = FirebaseAuth.instance.currentUser;
  //defining firebase storage
  firebase_storage.FirebaseStorage _storage =
      firebase_storage.FirebaseStorage.instanceFor(
          bucket: "fir-tu-29647.appspot.com");
  String downloadUrl =
      "https://png.pngtree.com/png-vector/20191026/ourlarge/pngtree-camera-icon-png-image_1871609.jpg";

  Map<String, dynamic> mapUser = new Map();
  File? _getPickedFile;

  void downloadFile() async {
    downloadUrl = await _storage
        .ref('${FirebaseAuth.instance.currentUser?.uid}/profilePhotos')
        .getDownloadURL();

    setState(() {});
  }

  @override
  initState() {
    super.initState();
    print("here we are again debug all the codes");
    WidgetsBinding.instance.addPostFrameCallback((_) {
      downloadFile();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: _firebaseFirestore
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Center(child: Scaffold(body: Text("Something went wrong")));
        }

        if (snapshot.hasData && !snapshot.data!.exists) {
          return Center(child: Scaffold(body: Text("Document does not exist")));
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;

          return Scaffold(
            appBar: AppBar(
              title: Text("welcome back  " + data['firstName']),
              backgroundColor: Colors.redAccent,
            ),
            body: LayoutBuilder(
              builder: (context, constraint) {
                return SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints:
                        BoxConstraints(minHeight: constraint.maxHeight),
                    child: IntrinsicHeight(
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 30,
                            ),
                            InkWell(
                              onTap: () async {
                                FilePickerResult? _filePickerResult =
                                    await FilePicker.platform.pickFiles(
                                  type: FileType.image,
                                  allowMultiple: false,
                                );
                                if (_filePickerResult == null) return;
                                _getPickedFile =
                                    File(_filePickerResult.files.first.path!);

                                try {
                                  final ref = _storage
                                      .ref(FirebaseAuth
                                          .instance.currentUser?.uid)
                                      .child("/profilePhotos");
                                  await ref.putFile(_getPickedFile!);
                                  downloadUrl = await ref.getDownloadURL();
                                  setState(() {
                                    downloadUrl;
                                  });

                                  _firebaseFirestore
                                      .collection("users")
                                      .doc(FirebaseAuth
                                          .instance.currentUser?.uid)
                                      .set({"profilePhoto": downloadUrl},
                                          SetOptions(merge: true));

                                  downloadFile();
                                } catch (e) {
                                  print(e);
                                }
                              },
                              child: CircleAvatar(
                                radius: 70,
                                backgroundImage:
                                    Image.network(downloadUrl).image,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text("welcome  " + data["firstName"]),
                            Expanded(child: Text("")),
                            Container(
                              alignment: Alignment.bottomRight,
                              child: FloatingActionButton(
                                backgroundColor: Colors.redAccent,
                                onPressed: () async {
                                  try {
                                    await _auth.signOut();
                                    ScaffoldMessenger.of(context)
                                        .hideCurrentSnackBar();
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(SnackBar(
                                      content: Text("Logout successful"),
                                    ));
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SignInScreen()));
                                  } on FirebaseAuthException catch (e) {
                                    ScaffoldMessenger.of(context)
                                        .hideCurrentSnackBar();
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(SnackBar(
                                      content: Text(e.toString()),
                                    ));
                                  }
                                },
                                child: Text(
                                  "SignOut",
                                  style: TextStyle(
                                    fontSize: 10,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        }
        return Scaffold(body: Center(child: Text("loading")));
      },
    );
  }
}
